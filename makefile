CC=gcc
CFLAGS= -g -Wall -pedantic -ansi
EXE=p4_e1 p4_e2 p4_e4

all: $(EXE) clear


p4_e1: p4_e1.o tree.o
	$(CC) $(CFLAGS) -o $@ p4_e1.o tree.o

p4_e2: p4_e2.o tree.o
	$(CC) $(CFLAGS) -o $@ p4_e2.o tree.o

p4_e4: p4_e2.o tree.o node_string.o
	$(CC) $(CFLAGS) -o $@ p4_e2.o tree.o node_string.o



p4_e1.o: p4_e1.c
	$(CC) $(CFLAGS) -c p4_e1.c

p4_e2.o: p4_e2.c
	$(CC) $(CFLAGS) -c p4_e2.c

p4_e4.o: p4_e4.c
	$(CC) $(CFLAGS) -c p4_e4.c

tree.o: tree.c tree.h
	$(CC) $(CFLAGS) -c tree.c

node_string.o: node_string.c node_string.h
	$(CC) $(CFLAGS) -c node_string.c


clear:
	rm -rf *.o

clean:
	rm -rf *.o $(EXE)

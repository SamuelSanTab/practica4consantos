#ifndef NODE_STRING_H_
#define NODE_STRING_H_
#include "types.h"


typedef struct _String String;


String *string_ini(char* name);

void string_destroy(String *n);

char * string_getName(const String *n);

int string_cmp (const String * n1, const String * n2);

String *string_copy(const String * src);

int string_print(FILE *pf, const String * n);

#endif

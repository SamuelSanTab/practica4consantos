#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "tree.h"


#define root(tree) ((tree)->root)
#define info(pnodo) ((pnodo)->info)
#define izq(pnodo) ((pnodo)->left)
#define der(pnodo) ((pnodo)->right)



typedef struct _NodeBT {
  void* info;
  struct _NodeBT* left;
  struct _NodeBT* right;
} NodeBT;

struct _Tree {
  NodeBT *root;

  destroy_element_function_type   destroy_element_function;
  copy_element_function_type      copy_element_function;
  print_element_function_type     print_element_function;
  cmp_element_function_type       cmp_element_function;
};

/*----------------------------------------------------------------------------*/
NodeBT* NodeBT_ini();
void NodeBT_free(NodeBT* node);

void tree_free_rec(NodeBT* node, destroy_element_function_type f);
Status tree_insert_rec(NodeBT** ppn, const void* pe, copy_element_function_type f1, cmp_element_function_type f2);
Status tree_preOrder_rec(FILE *f, NodeBT *pa, print_element_function_type print);
Status tree_inOrder_rec(FILE *f, NodeBT *pa, print_element_function_type print);
Status tree_postOrder_rec(FILE *f, NodeBT *pa, print_element_function_type print);
Bool tree_find_rec(NodeBT *pa, const void* pe, cmp_element_function_type f);
int tree_depth_rec(NodeBT *pa, int i);
int tree_numNodes_rec(NodeBT *pa, int i);

/*----------------------------------------------------------------------------*/



Tree* tree_ini(destroy_element_function_type f1, copy_element_function_type f2, print_element_function_type f3, cmp_element_function_type f4){
  Tree* New_Tree = NULL;

  New_Tree = (Tree*) malloc(sizeof(Tree));


  root(New_Tree) = NULL;
  New_Tree->destroy_element_function = f1;
  New_Tree->copy_element_function = f2;
  New_Tree->print_element_function = f3;
  New_Tree->cmp_element_function = f4;

  return New_Tree;
}

Bool tree_isEmpty(const Tree *pa){

  if(root(pa) == NULL){
    return TRUE;
  }
  return FALSE;
}

void tree_free(Tree *pa){
  if(pa == NULL){
    return;
  }

  tree_free_rec(root(pa), pa->destroy_element_function);

  free(pa);

}

Status tree_insert(Tree *pa, const void *pe){
  if(!pa|| !pe) return ERROR;

  return tree_insert_rec(&root(pa), pe, pa->copy_element_function, pa->cmp_element_function);
}

Status tree_preOrder(FILE *f, const Tree *pa){
  if(pa == NULL) return ERROR;


  return tree_preOrder_rec(f, root(pa), pa->print_element_function);

}

Status tree_inOrder(FILE *f, const Tree *pa){
  if(pa == NULL) return ERROR;

  return tree_inOrder_rec(f, root(pa), pa->print_element_function);

}

Status tree_postOrder(FILE *f, const Tree *pa){

  if(pa == NULL) return ERROR;

  return tree_postOrder_rec(f, root(pa), pa->print_element_function);

}

int tree_depth(const Tree *pa){
  if(pa == NULL) return -1;


  return tree_depth_rec(root(pa), -1);
}

int tree_numNodes(const Tree *pa){
  if(pa == NULL) return 0;

  return tree_numNodes_rec(root(pa), 0);
}

Bool tree_find(Tree* pa, const void* pe){
  if (pa == NULL || pe==NULL) return FALSE;

  return tree_find_rec(root(pa), pe, pa->cmp_element_function);
}




/*----------------------------------------------------------------------------*/
void tree_free_rec(NodeBT* node, destroy_element_function_type f){
  if (node == NULL)
    return;

  if (izq(node)) tree_free_rec(izq(node), f);

  if (der(node)) tree_free_rec(der(node), f);

  f(info(node));

  NodeBT_free(node);
}

Status tree_insert_rec(NodeBT** ppn, const void *pe, copy_element_function_type f1, cmp_element_function_type f2){

  int cmp;


  if(*ppn == NULL) {
    (*ppn) = NodeBT_ini();

    if(*ppn == NULL) return ERROR;

    (*ppn)->info = f1(pe);

  return OK;
  }
  cmp = f2(pe, info(*ppn));

  if(cmp < 0){
    return tree_insert_rec(&izq(*ppn), pe, f1, f2);
  }
  if(cmp > 0){
    return tree_insert_rec(&der(*ppn), pe, f1, f2);
  }

  return OK;
}

Status tree_preOrder_rec(FILE *f, NodeBT *pa, print_element_function_type print){
  if (pa == NULL) return ERROR;

  print(f, info(pa));

  tree_preOrder_rec(f, izq(pa), print);
  tree_preOrder_rec(f, der(pa), print);

return OK;

}

Status tree_inOrder_rec(FILE *f, NodeBT *pa, print_element_function_type print){
  if (pa == NULL) return ERROR;

  tree_inOrder_rec(f, izq(pa), print);

  print(f, info(pa));

  tree_inOrder_rec(f, der(pa), print);

return OK;

}

Status tree_postOrder_rec(FILE *f, NodeBT *pa, print_element_function_type print){


  if (pa == NULL) return ERROR;

  tree_postOrder_rec(f, izq(pa), print);
  tree_postOrder_rec(f, der(pa), print);

  print(f, info(pa));

return OK;

}

Bool tree_find_rec(NodeBT *pa, const void* pe, cmp_element_function_type f){
  if (pe==NULL || pa==NULL){
    return FALSE;
  }

  if (f(info(pa), pe) == 0){
    return TRUE;
  }

  if (tree_find_rec(izq(pa), pe, f) == TRUE)
    return TRUE;

  if (tree_find_rec(der(pa), pe, f) == TRUE)
    return TRUE;

  return FALSE;
}

int tree_depth_rec(NodeBT *pa, int i){
  int rigth, left;

  if (pa == NULL) return i;

  i++;

  rigth = left = 0;

  rigth = tree_depth_rec(der(pa), i);
  left = tree_depth_rec(izq(pa), i);

  if (rigth>=left)
    return rigth;
  else
    return left;

  return -1;
}

int tree_numNodes_rec(NodeBT *pa, int i){
  if (pa == NULL) return i;


  i++;

  i = tree_numNodes_rec(der(pa), i);

  i = tree_numNodes_rec(izq(pa), i);

  return i;
}
/*----------------------------------------------------------------------------*/




NodeBT* NodeBT_ini(){
  NodeBT* new_NodeBT;

  new_NodeBT = (NodeBT*) malloc(sizeof(NodeBT));

  info(new_NodeBT) = NULL;
  izq(new_NodeBT) = NULL;
  der(new_NodeBT) = NULL;

  return new_NodeBT;
}

void NodeBT_free(NodeBT* node){
  free(node);
}

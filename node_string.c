#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "node_string.h"

struct _String {
	char *str;
};

String *string_ini(char* name){
  String* new_string = NULL;
  int size = 0;
  if (name == NULL) return NULL;
  size = strlen(name);
  if (size == 0) return NULL;
  size++;
  new_string = (String*) calloc(1,sizeof(String));
  if (new_string == NULL) return NULL;
  new_string->str = (char*) calloc(size,sizeof(char));
  if (new_string->str == NULL){
    free(new_string);
    return NULL;
  }
  strcpy(new_string->str, name);
  return new_string;
}

void string_destroy(String *n){

  if (n == NULL) return;

  free(n->str);
  free(n);
}

char *string_getName(const String *n){

  if (n == NULL) return NULL;

  return n->str;
}

int string_cmp (const String * n1, const String * n2){

  if (n1 == NULL || n2 == NULL) return 0;

  return strcmp(n1->str, n2->str);
}

String *string_copy(const String * src){

  if (src == NULL) return NULL;
  return string_ini(src->str);
}

int string_print(FILE *pf, const String * n){
  int caracteres = 0;

	caracteres = fprintf(pf, "[%s] ",n->str);

	if(n==NULL){
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error opening file: %s\n", strerror(errno));
	};
	return caracteres;
}

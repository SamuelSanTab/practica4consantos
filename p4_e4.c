#include <stdio.h>
#include <stdlib.h>

#include "tree.h"
#include "node_string.h"
#include "types.h"

void str_free(void *n);
void *str_copy(const void *n);
int str_print(FILE *f, const void* n);
int str_compare(const void* n1, const void* n2);


int main(int argc, char const *argv[]){

  FILE* f = NULL;
  Tree* tree = NULL;
  char string[1000];
  int number = 0;
  String* temp_string = NULL;

  if (argc<2){
    printf("%s <file>\n", argv[0]);
  }
  f = fopen(argv[1], "r");
  tree = tree_ini(str_free, str_copy, str_print, str_compare);
  while (fscanf(f, "%s", string) > 0){
    temp_string = string_ini(string);
    tree_insert(tree, temp_string);
    string_destroy(temp_string);
  }

  number = tree_numNodes(tree);
  fprintf(stdout,"Numero de nodos: %d\n", number);
  number = tree_depth(tree);
  fprintf(stdout,"Profundidad: %d\n", number);

  tree_inOrder(stdout, tree);
  fprintf(stdout, "\n");

  fprintf(stdout,"Introduce una cadena para buscar en el árbol (siguiendo el mismo formato que en el fichero de entrada): ");
  scanf("%s", string);
  temp_string = string_ini(string);
  if (tree_find(tree, temp_string) == TRUE)
    fprintf(stdout,"Elemento encontrado!\n");
  else
    fprintf(stdout,"Elemento NO encontrado\n");

  string_destroy(temp_string);
  tree_free(tree);
  fclose(f);
	return 0;
}

void str_free(void *n){
  string_destroy((String*)n);
}

void *str_copy(const void *n){
  return string_copy((String*)n);
}

int str_print(FILE *f, const void* n){
  int i = 0;

  i = string_print(f, (String*)n);

  return i;
}

int str_compare(const void* n1, const void* n2){
  return string_cmp ((String*)n1, (String*)n2);
}

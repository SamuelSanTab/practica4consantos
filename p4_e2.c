#include <stdio.h>
#include <stdlib.h>

#include "tree.h"
#include "types.h"

void number_free(void *n);
void *number_copy(const void *n);
int number_print(FILE *f, const void* n);
int number_compare(const void* n1, const void* n2);


int main(int argc, char const *argv[]){

  FILE* f = NULL;
  Tree* tree = NULL;
  int number;

  if (argc<2){
    printf("%s <file>\n", argv[0]);
  }

  f = fopen(argv[1], "r");

  tree = tree_ini(number_free, number_copy, number_print, number_compare);

  while (fscanf(f, "%d", &number) > 0){
    tree_insert(tree, &number);
  }


  number = tree_numNodes(tree);
  fprintf(stdout,"Numero de nodos: %d\n", number);

  number = tree_depth(tree);
  fprintf(stdout,"Profundidad: %d\n", number);

  fprintf(stdout,"Orden previo:");
  tree_preOrder(stdout, tree);
  fprintf(stdout, "\n");

  fprintf(stdout,"Orden medio:");
  tree_inOrder(stdout, tree);
  fprintf(stdout, "\n");

  fprintf(stdout,"Orden posterior:");
  tree_postOrder(stdout, tree);
  fprintf(stdout, "\n");

  fprintf(stdout,"Introduzca un numero: ");
  scanf("%d", &number);

  fprintf(stdout,"Numero introducido: %d\n", number);

  if (tree_find(tree, &number) == TRUE)
    fprintf(stdout,"El dato %d se encuentra dentro del Arbol\n", number);
  else
    fprintf(stdout,"El dato NO %d se encuentra dentro del Arbol\n", number);


  tree_free(tree);

  fclose(f);

	return 0;
}

void number_free(void *n){
  free(n);
}

void *number_copy(const void *n){
  int *i = NULL;
  if (n == NULL) return NULL;
  i = (int*) malloc(sizeof(int));
  *i = *(int*)n;
  return i;
}

int number_print(FILE *f, const void* n){
  int i = 0;
  if (n != NULL){
    i = fprintf(f, " %d", *(int*)n);
  }
  return i;
}

int number_compare(const void* n1, const void* n2){

  if (n1 == NULL || n2 == NULL){
    return -1;
  }

  if (*(int*)n1>*(int*)n2) return 1;
  else if(*(int*)n1<*(int*)n2) return -1;
  else return 0;

  return 1;
}
